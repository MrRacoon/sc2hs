{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE AllowAmbiguousTypes        #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE UndecidableInstances       #-}
{-# LANGUAGE OverloadedLabels           #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-#LANGUAGE DerivingVia #-}
{-# LANGUAGE PolyKinds               #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE BangPatterns #-}

-- | Low level types used for more optimal storage of protocol messages.
module Network.SC2.LowLevel.Types
  ( module Network.SC2.LowLevel.Types
  , A.Difficulty(..)
  , A.Status(..)
  )
where

import           Control.Lens
import           Control.Lens.TH
import Control.Monad ( liftM )
import qualified Data.ByteString               as BS
import           Data.Coerce
import           Data.Functor.Identity
import           Data.ProtoLens                 ( defMessage )
import           Data.Text
import           Data.Void
import           Data.Word
import           Data.ProtoLens.Labels
import           Data.ProtoLens.Field
import           Network.SC2.Constants.Entities
import           Network.SC2.LowLevel.Convert
import Unsafe.Coerce (unsafeCoerce)
import qualified Proto.S2clientprotocol.Common as C
import qualified Proto.S2clientprotocol.Raw    as R
import qualified Proto.S2clientprotocol.Sc2api as A

import Data.Array.Accelerate as Accel hiding (Eq, fromIntegral, Enum, Bounded, Ord)
import Data.Array.Accelerate.IO.Data.ByteString
import Data.Array.Accelerate.IO.Codec.BMP
import Data.Array.Accelerate.Data.Colour.RGBA
import System.FilePath
import qualified Data.Array.Accelerate.Interpreter as InterpAccel

import qualified Data.Vector.Unboxed as VU
import qualified Data.Vector.Generic as VG
import qualified Data.Vector.Generic.Mutable as VGM
-- | A `Point` is a 2D/3D tuple of `Float`s. If 2d, the z coordinate is irrelevant.
type Point = (Float, Float, Float)
-- | A grid point.
type PointI = (Int, Int)

-- | Stores its pixel depth, dimension, and raw data.
data RawImageData  = RawImageData !Int !(Int, Int) !BS.ByteString
  deriving (Show, Eq)

toImage :: forall (ty :: SpecificImageType). ((ByteStrings (PixelType ty)) ~ BS.ByteString, Elt(PixelType ty))=> RawImageData -> Image ty
toImage (RawImageData d (w,h) bs) = ImageData $ (fbs  ( unsafeCoerce bs) ) where -- FIXME remove unsafeCoerce by using an equality proof
  fbs = fromByteStrings (Z :. h :. w)

toGreyscale' :: Exp Word8 -> Exp (RGBA Word8)
toGreyscale' m = lift $ RGBA m m m 255
toGreyscale :: Acc (Matrix Word8) -> Acc (Matrix (Word32))
toGreyscale =  Accel.map (packRGBA8 . toGreyscale')

saveImageGreyscale :: (PixelType k) ~ Word8 => FilePath -> Image k -> IO()
saveImageGreyscale name (ImageData a) = do
    let dat = InterpAccel.run $ toGreyscale (lift a)
    writeImageToBMP name dat


newtype Image (ty :: SpecificImageType) = ImageData {array :: Matrix (PixelType ty)}


data SpecificImageType = UnknownImageType | PathingGrid | TerrainHeight | PlacementGrid | Visibility | CreepSpread deriving (Eq, Show)
class ((ByteStrings (PixelType ty)) ~ BS.ByteString, Elt(PixelType ty)) => ImageDepth (ty :: SpecificImageType) where
  type PixelType ty
instance ImageDepth UnknownImageType where
  type PixelType UnknownImageType = Word8
instance ImageDepth PathingGrid where
  type PixelType PathingGrid = Word8
instance ImageDepth TerrainHeight where
  type PixelType TerrainHeight = Word8
instance ImageDepth PlacementGrid where
  type PixelType PlacementGrid = Word8
instance ImageDepth Visibility where
  type PixelType Visibility = Word8
instance ImageDepth CreepSpread where
  type PixelType CreepSpread = Word8



deriving instance (Elt (PixelType ty), Eq (PixelType ty)) => Eq (Image ty)
deriving instance Elt (PixelType ty) => Show (Image ty)
data Race' a = Terran
              | Zerg
              | Protoss
              | Random a
              deriving (Show, Eq)

type Race = Race' () -- ^ Can still be Random
type RaceResolved = Race' (Maybe (Race' Void)) -- ^ Either we don't know what the race is yet (Random) or we do.


type PlayerName = Text
data Interface c = Raw | Score | FeatureLayer c | Render c
                  deriving (Show, Eq)

newtype PlayerID = PlayerID Int
                  deriving (Show, Eq)

data PlayerType r = Observer
              | Participant r
              | Computer r A.Difficulty
              deriving (Show, Eq)

data Map = BattlenetMap Text
          | LocalMap Text (Maybe BS.ByteString)
          deriving (Show, Eq)


data ChatChannel = Broadcast | Team deriving (Eq, Show)

type RawTag = Word64

newtype UnitID = UnitID {unRawTag :: RawTag}
  deriving (Eq, Show, Ord, VU.Unbox)
  --deriving (VG.Vector VU.Vector, VGM.MVector VU.MVector, VU.Unbox) via RawTag


newtype instance VU.Vector UnitID = VG_UnitID (VU.Vector RawTag)
newtype instance VU.MVector s UnitID = VGM_UnitID (VU.MVector s RawTag)
instance VG.Vector VU.Vector UnitID where
  {-# INLINE basicUnsafeFreeze #-}
  {-# INLINE basicUnsafeThaw #-}
  {-# INLINE basicLength #-}
  {-# INLINE basicUnsafeSlice #-}
  {-# INLINE basicUnsafeIndexM #-}
  {-# INLINE elemseq #-}
  basicUnsafeFreeze (VGM_UnitID v) = VG_UnitID `liftM` VG.basicUnsafeFreeze v
  basicUnsafeThaw (VG_UnitID v) = VGM_UnitID `liftM` VG.basicUnsafeThaw v
  basicLength (VG_UnitID v) = VG.basicLength v
  basicUnsafeSlice i n (VG_UnitID v) = VG_UnitID $ VG.basicUnsafeSlice i n v
  basicUnsafeIndexM (VG_UnitID v) i = coerce `liftM` VG.basicUnsafeIndexM v i
  basicUnsafeCopy (VGM_UnitID mv) (VG_UnitID v) = VG.basicUnsafeCopy mv v
  elemseq _ = seq

instance VGM.MVector VU.MVector UnitID where
  {-# INLINE basicLength #-}
  {-# INLINE basicUnsafeSlice #-}
  {-# INLINE basicOverlaps #-}
  {-# INLINE basicUnsafeNew #-}
  {-# INLINE basicInitialize #-}
  {-# INLINE basicUnsafeReplicate #-}
  {-# INLINE basicUnsafeRead #-}
  {-# INLINE basicUnsafeWrite #-}
  {-# INLINE basicClear #-}
  {-# INLINE basicSet #-}
  {-# INLINE basicUnsafeCopy #-}
  {-# INLINE basicUnsafeGrow #-}
  basicLength (VGM_UnitID v) = VGM.basicLength v
  basicUnsafeSlice i n (VGM_UnitID v) = VGM_UnitID $ VGM.basicUnsafeSlice i n v
  basicOverlaps (VGM_UnitID v1) (VGM_UnitID v2) = VGM.basicOverlaps v1 v2
  basicUnsafeNew n = VGM_UnitID `liftM` VGM.basicUnsafeNew n
  basicInitialize (VGM_UnitID v) = VGM.basicInitialize v
  basicUnsafeReplicate n x = VGM_UnitID `liftM` VGM.basicUnsafeReplicate n (coerce x)
  basicUnsafeRead (VGM_UnitID v) i = coerce `liftM` VGM.basicUnsafeRead v i
  basicUnsafeWrite (VGM_UnitID v) i x = VGM.basicUnsafeWrite v i (coerce x)
  basicClear (VGM_UnitID v) = VGM.basicClear v
  basicSet (VGM_UnitID v) x = VGM.basicSet v (coerce x)
  basicUnsafeCopy (VGM_UnitID v1) (VGM_UnitID v2) = VGM.basicUnsafeCopy v1 v2
  basicUnsafeMove (VGM_UnitID v1) (VGM_UnitID v2) = VGM.basicUnsafeMove v1 v2
  basicUnsafeGrow (VGM_UnitID v) n = VGM_UnitID `liftM` VGM.basicUnsafeGrow v n
--deriving instance VU.Unbox (UnitID)
type Alliance = R.Alliance



type IdentifiesUnit a = HasField a "unitTag" RawTag

-- | Multiple things can be a target.
data Target where
  TargetPoint ::Point -> Target
--  TargetScreen :: PointI -> Target
--  TargetMinimap :: PointI -> Target


  TargetUnit ::UnitID -> Target

deriving instance Eq Target
deriving instance Show Target
instance ConvertProto C.Point2D where
  type Unproto C.Point2D = Point
  convertTo (!x, !y, _) = defMessage & #x .~ x & #y .~ y
  convertFrom p = (,,) <$> p ^. #maybe'x <*> p ^. #maybe'y <*> Just 0

instance ConvertProto C.Point where
  type Unproto C.Point = Point
  convertTo (!x, !y, !z) = defMessage & #x .~ x & #y .~ y & #z .~ z
  convertFrom p = (,,) <$> p ^. #maybe'x <*> p ^. #maybe'y <*> p ^. #maybe'z


-- | Something you can target with an ability.
class Targetable a where
  asTarget :: a -> Target

instance Targetable Point where
  asTarget = TargetPoint


instance Targetable Target where
  asTarget = id

-- | A message asking for game information.
data GameInfo = GameInfo
              deriving (Show, Eq)

data PlayerInfo =
  PlayerInfo
  {
    playerID   :: PlayerID,
    playerType :: PlayerType RaceResolved,
    playerName :: PlayerName
  } deriving (Show, Eq)
data GameInfoResponse =
  GameInfoResponse
  {
    mapName      :: Text,
    modNames     :: [Text],
    localMapPath :: FilePath,
    playerInfo   :: [PlayerInfo],
    startRaw     :: Maybe StartRaw,
    interfaces   :: [Interface ()]
  } deriving (Eq, Show)

data StartRaw =
  StartRaw
  {
    mapSize        :: !(Int, Int),
    pathingGrid    :: !(Image PathingGrid),
    terrainHeight  :: !(Image TerrainHeight),
    placementGrid  :: !(Image PlacementGrid),
    playableArea   :: !((Int, Int), (Int, Int)),
    startLocations :: ![Point]
  } deriving (Eq, Show)

data PlayerResources = PlayerResources




data MapInfo = MapInfo

-- | Power sources for Protoss units.
data PowerSource = PowerSource {
      _position :: !Point,
      _radius   :: !Float,
      _tag      :: !RawTag
    } deriving (Eq, Show)
makeFieldsNoPrefix ''PowerSource

newtype GroupID = GroupID Word deriving (Eq, Show, Enum, Bounded)

{-#INLINE convertRace #-}
convertRace :: Race' a -> C.Race
convertRace Terran     = C.Terran
convertRace Zerg       = C.Zerg
convertRace Protoss    = C.Protoss
convertRace (Random _) = C.Random

{-#INLINE convertRaceBack #-}
convertRaceBack :: C.Race -> a -> Race' a
convertRaceBack C.Terran  = const Terran
convertRaceBack C.Zerg    = const Zerg
convertRaceBack C.Protoss = const Protoss
convertRaceBack C.Random  = Random

{-#INLINE convertPlayerType #-}
convertPlayerType :: PlayerType (Race' a) -> A.PlayerSetup
convertPlayerType Observer = defMessage & #type' .~ A.Observer
convertPlayerType (Participant r) =
  defMessage & #type' .~ A.Participant & #race .~ convertRace r --A.playerSetup (Just A.Participant) (Just (convertRace r)) Nothing


convertPlayerType (Computer r d) =
  defMessage & #type' .~ A.Computer & #race .~ convertRace r & #difficulty .~ d--A.playerSetup (Just A.Computer) (Just (convertRace r)) (Just d)


instance ConvertProto C.PointI where
  type Unproto C.PointI = (Int, Int)
  convertTo (!x, !y) = defMessage & #x .~ fromIntegral x & #y .~ fromIntegral y
  convertFrom p =
    (,)
      <$> (fromIntegral <$> p ^. #maybe'x)
      <*> (fromIntegral <$> p ^. #maybe'y)--(fromIntegral p^.x, fromIntegral p^.y)



instance ConvertProto C.RectangleI where
  type Unproto C.RectangleI = ((Int, Int), (Int, Int))
  convertTo (!a, !b) = defMessage & #p0 .~ convertTo a & #p1 .~ convertTo b
  convertFrom !r =
    (,)
      <$> (convertFrom =<< r ^. #maybe'p0)
      <*> (convertFrom =<< r ^. #maybe'p1)


instance ConvertProto C.Size2DI where
  type Unproto C.Size2DI = (Int, Int)
  convertTo (!x, !y) = defMessage & #x .~ fromIntegral x & #y .~ fromIntegral y
  convertFrom sz =
    (,)
      <$> (fromIntegral <$> sz ^. #maybe'x)
      <*> (fromIntegral <$> sz ^. #maybe'y)

instance ConvertProto C.ImageData where
  type Unproto C.ImageData = RawImageData -- TODO try and create the correct image types
  convertTo (RawImageData bits size dat) =
    defMessage
      &  #bitsPerPixel
      .~ fromIntegral bits
      &  #size
      .~ convertTo size
      &  #data'
      .~ dat
  convertFrom i =
    RawImageData
      <$> (fromIntegral <$> i ^. #maybe'bitsPerPixel)
      <*> (convertFrom =<< i ^. #maybe'size)
      <*> i
      ^.  #maybe'data'


extractTypedImage !r !l =  do
      rawbytes <- (r^. l)
      rawimg <- convertFrom rawbytes
      return (toImage rawimg)
instance ConvertProto R.ActionRawUnitCommand'Target where
  type Unproto R.ActionRawUnitCommand'Target = Target
  convertTo (TargetPoint !p) =
    R.ActionRawUnitCommand'TargetWorldSpacePos (convertTo p)
  convertTo (TargetUnit !u) = R.ActionRawUnitCommand'TargetUnitTag (coerce u)
  convertFrom = error "ConvertFrom ActionRawUnitCommand'Target TODO" -- TODO


type Health = Float
type ShieldPower = Float
type Energy = Float
type Angle = Float
type Distance = Float
type UnitInterval = Float -- ^ A point on the closed unit interval [0,1]