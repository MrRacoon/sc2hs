{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedLabels           #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeSynonymInstances       #-}

module Network.SC2.LowLevel.Unit where
import           Control.Lens
import           Data.Int(Int32)
import           Control.Lens.TH
import           Data.Coerce
import           Data.Maybe
import           Data.ProtoLens                  (defMessage)
import           Network.SC2.Constants.Abilities as Abilities
import           Network.SC2.Constants.Entities
import           Network.SC2.Constants.Units     as Units
import           Network.SC2.Constants.Buffs as Buffs (fromInt)
import           Network.SC2.LowLevel.Convert
import           Network.SC2.LowLevel.Types
import qualified Proto.S2clientprotocol.Common   as C
import qualified Proto.S2clientprotocol.Raw      as R

import qualified Data.Vector as V
import qualified Data.Vector.Generic as VG
import qualified Data.Vector.Unboxed as VU


data DisplayType = Visible -- ^ The unit can be directly seen
                    | Snapshot -- ^ The unit exists in the fog of war, such as a known enemy structure
                    | Hidden -- ^ Can't see it.
                    deriving (Eq, Show)

{-#INLINE dt2dt #-}
dt2dt :: R.DisplayType -> DisplayType
dt2dt R.Visible = Visible
dt2dt R.Snapshot = Snapshot
dt2dt R.Hidden = Hidden
data CloakState = CloakedUnknown | Cloaked | CloakedDetected | NotCloaked | CloakedAllied deriving (Eq, Show)

-- | Structure to issue orders to units.
data IssueOrder = IssueOrder {
    _abilityID :: AbilityType,
    _target    :: Target,
    _queue     :: Bool -- ^ Whether to add to the queue
} deriving (Eq, Show)

makeFieldsNoPrefix ''IssueOrder

-- | Viewing a unit's current orders
data UnitOrder = UnitOrder {
    _abilityID :: AbilityType,
    _target    :: Target,
    _progress  :: Maybe UnitInterval -- ^ E.g. train progress
} deriving (Eq, Show)

makeFieldsNoPrefix ''UnitOrder



instance ConvertProto R.UnitOrder where
    type Unproto R.UnitOrder = UnitOrder
    convertTo o = defMessage @R.UnitOrder & #abilityId .~ Abilities.toInt (o^. abilityID) & setTarget (o ^. target @UnitOrder)
                        & #maybe'progress .~ (o ^. progress)
                            where
                                setTarget (TargetPoint p) m = m & #targetWorldSpacePos .~ convertTo p
                                setTarget (TargetUnit u) m = m & #targetUnitTag .~ (coerce u :: RawTag)
    convertFrom _ = undefined


--TODO Add extra definitions
instance ConvertProto R.CloakState where
    type Unproto R.CloakState = CloakState
    convertTo Cloaked         = R.Cloaked
    convertTo CloakedDetected = R.CloakedDetected
    convertTo NotCloaked      = R.NotCloaked
    convertFrom R.Cloaked         = Just Cloaked
    convertFrom R.CloakedDetected = Just CloakedDetected
    convertFrom R.NotCloaked      = Just NotCloaked

data UnitVitals = UnitVitals {
    _health    :: Health,
    _healthMax :: Health,
    _shield    :: ShieldPower,
    _shieldMax :: ShieldPower,
    _energy    :: Energy,
    _energyMax :: Energy
} deriving (Eq, Show)
makeClassyFor "HasUnitVitals" "vitalslol" [("_health", "health"), ("_healthMax", "healthMax"), --TODO "vitalslol"
                                        ("_shield", "shield"), ("_shieldMax", "shieldMax"),
                                        ("_energy", "energy"), ("_energyMax", "energyMax") ] ''UnitVitals
data PassengerUnit = PassengerUnit {
    _tag         :: UnitID
    , _unitType  :: UnitType
    , _vitals :: UnitVitals

} deriving (Eq, Show)
makeFieldsNoPrefix ''PassengerUnit

instance ConvertProto R.PassengerUnit where
    type Unproto R.PassengerUnit = PassengerUnit
    convertTo p = defMessage @R.PassengerUnit & #tag .~ unRawTag (p ^. tag) & error "Unimplemented: convertTo R.PassengerUnit"
    convertFrom m = Just PassengerUnit {..} where
        _tag = UnitID $ m ^. #tag
        _unitType = Units.fromInt (m ^. #unitType)
        _vitals = UnitVitals {..}
        _health = m ^. #health
        _healthMax = m ^. #healthMax
        _shield  = m ^. #shield
        _shieldMax  = m ^. #shieldMax
        _energy  = m ^. #energy
        _energyMax  = m ^. #energyMax

data Unit = Unit {
                    _tag           :: !UnitID,
                    _unitType      :: !UnitType,
                    _alliance      :: !Alliance,
                    _displayType   :: DisplayType,
                    _owner         :: !PlayerID,
                    _position      :: Point,
                    _facing        :: Angle,
                    _radius        :: Distance,
                    _buildProgress :: UnitInterval,
                    _cloak         :: CloakState,
                    _detectRange   :: Distance,
                    _radarRange    :: Distance,
                    _isSelected    :: Bool,
                    _isOnScreen    :: Bool,
                    _isBlip        :: Bool,
                    _isPowered     :: Bool,
                    _isActive      :: Bool,
                    _buffs :: [BuffType],

                    -- Not populated for snapshots
                    _vitals :: Maybe UnitVitals,
                    _mineralContents :: Maybe Int32,
                    _vespeneContents :: Maybe Int32,
                    _isFlying :: Maybe Bool,
                    _isBurrowed :: Maybe Bool,
                    _isHallucination :: Maybe Bool,

                    -- Not populated for enemies
                    _orders      :: [UnitOrder],
                    _addOnTag    :: Maybe UnitID,
                    _passengers  :: [PassengerUnit],
                    _cargoSpaceTaken :: Maybe Int32,
                    _cargoSpaceMax :: Maybe Int32,
                    _assignedHarvesters :: Maybe Int32,
                    _idealHarvesters :: Maybe Int32,
                    _weaponCooldown :: Maybe Float,
                    _engagedTarget :: Maybe UnitID
                } deriving (Eq, Show)
makeFieldsNoPrefix ''Unit





instance ConvertProto R.Unit where
    type Unproto R.Unit = Unit
    convertTo _ = error "Why are you converting Unit back to proto"
    convertFrom m = Just Unit {..} where
        _tag = UnitID (m ^. #tag)
        _unitType = Units.fromInt (m ^. #unitType)
        _alliance = m ^. #alliance
        _displayType = dt2dt $ m ^. #displayType
        _owner = PlayerID $ fromIntegral (m ^. #owner)
        _position = (fromJust . convertFrom) $ m ^. #pos
        _facing = m ^. #facing
        _radius = m ^. #radius
        _buildProgress = m ^. #buildProgress
        _cloak = fromJust . convertFrom $   m ^. #cloak
        _detectRange = m ^. #detectRange
        _radarRange = m ^. #radarRange
        _isSelected = m ^. #isSelected
        _isOnScreen = m ^. #isOnScreen
        _isBlip = m ^. #isBlip
        _isPowered = m ^. #isPowered
        _isActive = m ^. #isActive
        _vitals = if _displayType == Snapshot then Nothing else Just UnitVitals {..}
        _health = m ^. #health
        _healthMax = m ^. #healthMax
        _shield  = m ^. #shield
        _shieldMax  = m ^. #shieldMax
        _energy  = m ^. #energy
        _energyMax  = m ^. #energyMax
        _orders  = fmap (fromJust . convertFrom) $ m ^. #orders
        _addOnTag = fmap UnitID (m ^. #maybe'addOnTag)
        _passengers = fmap (fromJust . convertFrom) $ m ^. #passengers
        _isFlying = m ^. #maybe'isFlying
        _isBurrowed = m ^. #maybe'isBurrowed
        _isHallucination = m ^. #maybe'isHallucination
        _cargoSpaceTaken  = m ^. #maybe'cargoSpaceTaken
        _cargoSpaceMax = m ^. #maybe'cargoSpaceMax
        _buffs = fmap Buffs.fromInt $ m ^. #buffIds
        _vespeneContents = m ^. #maybe'vespeneContents
        _mineralContents = m ^. #maybe'mineralContents
        _assignedHarvesters = m ^. #maybe'assignedHarvesters
        _idealHarvesters = m ^. #maybe'idealHarvesters
        _weaponCooldown = m ^. #maybe'weaponCooldown
        _engagedTarget = coerce $ m ^. #maybe'engagedTargetTag


instance Targetable Unit where
    {-#INLINE asTarget #-}
    asTarget = TargetUnit . view tag

-- | Just a bunch of units, not necessarily selected. For example, it could be the list of all known units.
type UnitContainer = V.Vector Unit

data Units where
    ControlGroup :: GroupID ->  Units
    Ungrouped :: UnitContainer -> Units -- ^ Just a bunch of units.


{-#INLINE unitsToTags #-}
unitsToTags :: Units -> V.Vector RawTag
unitsToTags (Ungrouped u) = fmap (coerce . view tag) u

{-#INLINE individualUnit #-}
individualUnit :: Unit -> Units
individualUnit u = Ungrouped (V.singleton u)

-- | Something you can issue orders to.
class Orderable a where
    toIDs :: a -> VU.Vector UnitID
    -- ^ Grab the tags of the units to issue orders to

instance Orderable Units where
    {-#INLINE toIDs #-}
    toIDs (Ungrouped u) = VU.convert $  fmap (view tag) u
instance Orderable Unit where
    {-#INLINE toIDs #-}
    toIDs u = VG.singleton(view tag u)
instance Orderable UnitContainer where
    {-#INLINE toIDs #-}
    toIDs u = VU.convert $ fmap (view tag) u

{-#INLINE toTags #-}
toTags :: Orderable a => a -> VU.Vector RawTag
toTags =  coerce . toIDs


type UnitKey = UnitID
type Building = Unit
type MineralPatch = Unit
type Geyser = Unit
type CollapsableTerrain = Unit
type DestroyableTerrain = Unit
type EnemyUnit = Unit
type XelNagaTower = Unit
type NeutralUnit = Unit
type AlliedUnit = Unit