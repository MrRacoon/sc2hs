{-#LANGUAGE TemplateHaskell #-}
{-#LANGUAGE RankNTypes #-}
{-#LANGUAGE StandaloneDeriving #-}
{-#LANGUAGE FlexibleInstances#-}
module Network.SC2.Constants.Units where 
    
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import GHC.TypeLits
import Control.Lens.TH

import Network.SC2.Internal.ConstantGenerator

$(scUnitsDeclarations)
makePrisms ''UnitType