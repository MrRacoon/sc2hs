{-#LANGUAGE RankNTypes #-}
{-#LANGUAGE FlexibleInstances#-}
module Network.SC2.Constants.Entities (
  Network.SC2.Constants.Abilities.AbilityType,
  Network.SC2.Constants.Buffs.BuffType,
  Network.SC2.Constants.Effects.EffectType,
  Network.SC2.Constants.Units.UnitType,
  Network.SC2.Constants.Upgrades.UpgradeType
) where
    
import Network.SC2.Constants.Abilities
import Network.SC2.Constants.Buffs
import Network.SC2.Constants.Effects
import Network.SC2.Constants.Units
import Network.SC2.Constants.Upgrades